package typyProste;

import java.util.Scanner;

public class Exercise1 {

    private final int numberOfClasses;
    private final int numberOfNotes;
    private int[][] notes;


    public static void main(String[] args) {
        int numberOfClasses = 3;
        int numberOfNotes = 4;
        Exercise1 user1 = new Exercise1(numberOfClasses, numberOfNotes);
        user1.getNotes();
        user1.generateAverage();
    }


    public Exercise1 (int numberOfClasses, int numberOfNotes) {
        this.numberOfClasses = numberOfClasses;
        this.numberOfNotes = numberOfNotes;
        generateNotesTable();
    }

    private void generateNotesTable() {
        notes = new int[numberOfClasses][];
        for(int index = 0; index < notes.length; index++) {
            notes[index] = new int[numberOfNotes]; //the table will be symmetrical with length 4, creating the skeleton
        }
    }

    public void getNotes() {
        Scanner inputScanner = new Scanner(System.in); // object for obtaining data from the user
        for (int classIndex = 0; classIndex < numberOfClasses; classIndex++) {
            for (int noteIndex = 0; noteIndex < numberOfNotes; noteIndex++) {
                System.out.println("Input grade number " + (noteIndex + 1) + " from subject number " + (classIndex + 1) + ": ");
                notes[classIndex][noteIndex] = inputScanner.nextInt();
            }
        }
    }

    public void generateAverage() {
        float totalAverage = 0;
        for (int classIndex = 0; classIndex < numberOfClasses; classIndex++) {
            float classAverage = 0;
            for (int note : notes[classIndex]) {
                classAverage += note;
            }
            classAverage /= numberOfNotes; //calculating average for given subject
            System.out.println("The average for subject " + (classIndex + 1) + " is: " + classAverage);
            totalAverage += classAverage;
        }
        totalAverage /= numberOfClasses;
        System.out.println("Total average for all subjects is: " + totalAverage);
    }



}