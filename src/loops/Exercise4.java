package loops;

public class Exercise4 {
    public static void main(String[] args) {
        int number = 40;
        while (number > -10) {
            if (number % 2 != 0) {
                System.out.println(number);

            }
            number--;
        }

    }
}
