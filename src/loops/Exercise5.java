package loops;

public class Exercise5 {
    public static void main (String[] args){
        int[] numbers = new int[] {1,2,3,4};
        System.out.println(sumOfNumbers(numbers));
    }

    private static int sumOfNumbers(int ... numbers){
        int sum = 0;
        for (int number : numbers){
            sum += number;
        }
        return sum;
    }
}