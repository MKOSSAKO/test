package cast;


import java.util.NoSuchElementException;
import java.util.Scanner;


public class Exercise1 {
    public static void main (String[] args ) {

        Scanner scannerInput = new Scanner(System.in);
        int userNumber = getNumber(scannerInput);
        System.out.println(userNumber * Math.PI);
        System.out.println((int) (userNumber * Math.PI));
    }

    public static int getNumber(Scanner scannerInput) {
        System.out.println("Please insert the number");
        while(true) {
            try {
                return scannerInput.nextInt();
            }
            catch (NoSuchElementException | IllegalStateException exception) {
                System.out.println("You input wrong number, please try again");
                scannerInput.next();
            }
        }
    }


}

