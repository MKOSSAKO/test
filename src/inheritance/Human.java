package inheritance;
import java.util.Scanner;

public class Human extends Mammal {
    public String toString() {
        return "Hello Adrian";
    }

    public static void main(String[] args) {

        Scanner scannerInput = new Scanner(System.in);
        Human marta = new Human();

        System.out.println(marta.toString());

    }
}
