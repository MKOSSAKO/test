package inheritance;

public abstract class Mammal extends Animal {
    public abstract String toString() ;
}
