package inheritance;

public abstract class Fish extends Animal {
    public abstract String toString() ;
}
