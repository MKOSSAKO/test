package Exception;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise {



   public static void main(String[] args) {

       Scanner inputScanner = new Scanner(System.in);
       Exercise main = new Exercise();

       System.out.println("Please insert the number");

       double userNumber = 0;


       while(true) { //one can also set a variable to false and at the end set the variable to true
            try{
                userNumber = inputScanner.nextDouble();
                break;
           }
           catch (InputMismatchException exception) {
               System.out.println("The input should be a number");
               inputScanner.next();
           }
       }

       if (userNumber < 0){
          throw new IllegalArgumentException("The number must be greater than 0");
       }

        System.out.println("The result is " + main.compute(userNumber));
   }


    private double compute(double argument1){
        return java.lang.Math.sqrt(argument1);
    }


}