package heap;

import java.util.Scanner;

public class Exercise {

    public static long factorFunc (long n) {
        if (n <= 1) {
            return 1;
        }
        else {
            return n*factorFunc(n-1);
        }
    }

    public static void main(String[] args) {

        Scanner scannerInput = new Scanner(System.in);
        System.out.println("Please insert for which number the factorial should be calculated.");
        long n = scannerInput.nextLong();


        System.out.println("factorial at " + n + " is equal to " + factorFunc(n));
    }
}