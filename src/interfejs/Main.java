package interfejs;

import java.util.Scanner;

public class Main {

    private Scanner inputScanner = new Scanner(System.in);

    public static void main(String[] args) {
        Main main = new Main();
        Computation computation;

        if (main.shouldMultiply() ==  'M') {
            computation = new Multiplication(); // multiplies two input values
        }
        else {
            computation = new Addition();
        }

        double argument1 = main.getArgument();
        double argument2 = main.getArgument();

        double result = computation.compute(argument1, argument2);
        System.out.println("Wynik: " + result);
    }

    private char shouldMultiply() {
        System.out.println("Which computation would you like to do? If you want to multiply two numbers, please insert M, if you want to add, please enter A" );
        return inputScanner.next().charAt(0); //
    }

    private double getArgument() {
        System.out.println("Input number");
        return inputScanner.nextInt();
    }
}