package interfejs;

public class Addition implements Computation {
    @Override //override informs compilator that the method was taken from the parent
    public double compute(double argument1, double argument2){
        return argument1 + argument2;
    }
}